# Raspeberry Pi 5 with home-assistant.io

## Installation

- [ ] Flash your device with Ubuntu 23.04 or another Linux variant that is supported by the Raspeberry Pi. This can be done by using the [Raspberry Pi Imager](https://www.raspberrypi.com/software/). 

_It is recommended not to use a SD card, but instead use a SSD disk or similar over USB which can be plugged into the computer directly and flashed using the Raspberry Pi Imager._


### Install Docker

- [ ] Install docker on your device.

```
sudo apt-get update -y
sudo apt-get upgrade -y
sudo ufw disable
sudo apt install network-manager

sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```


### Install Docker Compose

- [ ] Install doker compose on non Ubuntu 24.04 installations.

```
sudo apt install docker-compose
```

- [ ] Install docker compose on Ubuntu 24.04.

_This is needed for Ubuntu 24.04 LTS, Docker Compose version v2.27.1 and Docker version 26.1.4 due to Python errors that show up when trying to start up the docker instances described in the docker compose file._

```
curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose
```

Please see the [guide](https://wiki.crowncloud.net/?How_to_Install_and_use_Docker_Compose_on_Ubuntu_24_04) for more details.


### Install timesync daemon

- [ ] Uninstall systemd-timesyncd as it conflicts

```
sudo apt remove systemd-timesyncd
```

- [ ] Install packages for ntpd timesync daemon.

```
sudo timedatectl set-ntp no
sudo apt install ntp
```

- [ ] Create an empty file `touch /etc/ntp.conf`.
- [ ] Add the content below into the file.

```
pool 0.ubuntu.pool.ntp.org iburst
pool 1.ubuntu.pool.ntp.org iburst
pool 2.ubuntu.pool.ntp.org iburst
pool 3.ubuntu.pool.ntp.org iburst
```

- [ ] Start the daemon.

```
sudo systemctl restart ntp
sudo systemctl enable ntp
```


### Install sqlite3

```
sudo apt install sqlite3
```

This is only needed for direct access to the database used by home-assistant.


### Install Home Assistant

- [ ] Build and start the docker container for home-assistant. Below the timezone ia set to `Europe/Berlin` and the configuration library is set to `/etc/home-assistant`. 

```
docker run -d \
  --name homeassistant \
  --privileged \
  --restart=always \
  -e TZ=Europe/Berlin \
  -v /etc/home-assistant:/config \
  -v /run/dbus:/run/dbus:ro \
  --network=host \
  ghcr.io/home-assistant/home-assistant:latest
```

- [ ] Restart Home Assistant.

```
docker restart homeassistant 
```

- [ ] Access Home Assistant  
  - Web interface: http://localhost:8123/
  - API: http://localhost:8123/api


### Configure Home Assistant to auto-start

- [ ] Create an empty file `touch /etc/init.d/homeassistant`.
- [ ] Add the content below into the file.

```
Description "homeassistant"
start on filesystem and started docker
stop on runlevel [!2345]
respawn
script
   /usr/bin/docker start -a homeassistant
end script
pre-stop script
  /usr/bin/docker stop homeassistant
end script 
```


## Updating Home Assistant in Docker

 - [ ] Stop the current container.
 - [ ] Delete the stopped container.
 - [ ] Delete the old home-assistant images.
 - [ ] Pull the new image from the docker hub with the `latest` tag.
 - [ ] Re-build and start the container (see above) with the `latest` tag.

```
docker stop homeassistant
docker rm homeassistant
docker image remove ghcr.io/home-assistant/home-assistant:latest
docker image remove homeassistant/home-assistant:latest
docker pull ghcr.io/home-assistant/home-assistant:latest
```

Please see the illustrative [examples](https://github.com/home-assistant/core/pkgs/container/home-assistant) on how to update home-assistant docker image, Avoid zero-versions, and use versions that are released mid-end of the month.

_The config directory is not in the container, so it remains untouched._


## Install Home Assistant Community Service in Docker 

Installing HACS can be tricky in Docker, but the documentation is very straightforward when you know how to read. Be aware that this step is required after each upgrade of home assistant if it is done as outlined above hence the installation of HACS is inside the container itself.

```
sudo docker exec -it homeassistant bash 
cd
wget -O - https://get.hacs.xyz | bash -
```

Restart Home Assistant respectively as it says in the info. This can be done by quitting the bash session with the `exit` command, and then restart the container as described above.


## Install MQTT Server

 - [ ] Create base folder for mqtt configuration.

```
mkdir -p mqtt5/config
cd mqtt5
```

 - [ ] Create an empty file `touch config/mosquitto.conf` for the Mosquitto configuration.
 - [ ] Insert the content below into the file to configure a broker with _anonymous_ access.

```
allow_anonymous true
listener 1883
listener 9001
protocol websockets
persistence true
# password_file /mosquitto/config/pwfile
persistence_file mosquitto.db
persistence_location /mosquitto/data/
```

 - [ ] Create an empty file `touch config/pwfile` for credentials (not used when anonymous access is enabled).
 - [ ] Create docker-compose file `touch docker-compose.yml`.
 - [ ] Insert the content below in the docker-compose.yml docker-compose file.

```
version: "3.7"
services:
  mqtt5:
    image: eclipse-mosquitto
    container_name: mqtt5
    ports:
      - "1883:1883"
      - "9001:9001"
    volumes:
      - ./config:/mosquitto/config:rw
      - ./data:/mosquitto/data:rw
      - ./log:/mosquitto/log:rw
    restart: unless-stopped
volumes:
  config:
  data:
  log:
networks:
  default:
    name: mqtt5-network
```

 - [ ] Create and run docker container for MQTT.

```
sudo docker-compose -p mqtt5 up -d
```

See also [Setup mosquitto with docker](https://github.com/sukesh-ak/setup-mosquitto-with-docker).


### Configure MQTT to auto-start

 - [ ] Create an empty file `touch /etc/init.d/mqtt`.
 - [ ] Add the content below into the file.

```
Description "mosquitto"
start on filesystem and started docker
stop on runlevel [!2345]
respawn
script
   /usr/bin/docker start -a mqtt5
end script
pre-stop script
  /usr/bin/docker stop mqtt5
end script 
```


### Install client tools

 - [ ] Install client tools for testing.

```
sudo apt install mosquitto-clients
```

 - [ ] Listen to topic

```
mosquitto_sub -v -t 'hello/topic' 
```


## Install and configure Watts Live 

Please see [Watts Live ind i MQTT og Home Assistant](https://smarthjemmet.dk/2023/09/watts-live-ind-i-mqtt-og-home-assistant/).

Additionally, please note that when storing the port to be used to access the MQTT message queue, you need to 
use the type _ByteArray_ and store the value as hexidecimal. Default port is 1883 (075B).


# License

This installation package is licensed under the MIT opensource license.

